## Exploring Modules

 - Modules are groups of similar functions that are used across many scripts 

 - You can see modules imported into your current session with "Get-Module"

 To see all of the exported commands within a module:
 ```
Get-Command -Module Microsoft.PowerShell.Management
 ```

 To see all modules that are available (both imported and not imported):
 ```
Get-Module -ListAvailable
Import-Module -Name 
Remove-Module -Name
 ```
- Microsoft Modules: "C:\Windows\system32\WindowsPowerShell\v1.0\Modules"
- User Modules: "C:\Program Files\WindowsPowerShell\Modules"
- Current User Modules: "C:\users\<LoggedInUser>\Documents\WindowsPowerShell\Modules"

- You can add module paths by modifying the $env:PSModulePath variable

- Modules are stored within a .psm1 file
	- These files are typically filled with functions and should follow the same verb-noun syntax
```
function Get-Software {
	param()
}

function Install-Software {
	param()
}

function Remove-Software {
	param()
}
```

# The Module Manifest

 - A module manifest is an optional but recommended text file written in the form of a Powershell hashtable
 	- The hashtable contains elements that describe metadata about the module 

 - A module manifest is a file with a .psd1 extension

 - You can create a module manifest from scratch, or have powershell generate a template:
 ```
New-ModuleManifest -Path 'C:\Program Files\WindowsPowershell\Modules\software\manifest.psd1' -Author 'Bad_Jubies'
-RootModule Software.psm1 -Description 'This module helps deploy software'

 ```

# Working with custom modules

- powershellgallery.com is a repo of thousands of Powershell modules 

- Powershell has a built in module to interact with Powershell Gallery "PowershellGet"

```
Get-Command -Module PowerShellGet
```
- You can use "Find-Module" to filter all modules i nthe gallery:
```
Find-Module -Name VMware*
```
- To install:
```
Find-Module -NAme VMware.PowerCLI | Install-Module
```
- To check the path of a module:
```
Get-Module -Name VMware.PowerCLI -ListAvailable | Select-Object -Property ModuleBase
```
- Uninstalling Modules:
```
Uninstall-Module -Name VMware.PowerCLI
```

# Creating your own module 

- Create the module folder:
```
mkdir 'C:\Program Files\WindowsPowerShell\Modules\MyModule'
```
- Add a blank .psm1 that will eventually hold your functions:
```
Add-Content 'C:\Program Files\WindowsPowerShell\Modules\MyModule\Software.psm1'
```
 - Create a module manifest:
 ```
New-ModuleManifest -Path 'C:\Program Files\WindowsPowerShell\Modules\MyModule'
 ```
- To see exported commands:
```
Get-Module -Name Sofware -List
```

