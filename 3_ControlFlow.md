## Control Flow

- Control flow allows your script to change based on the type of value that is supplied

# Using Conditional Statements 

- You can write expressions by using "comparison operators", which compare values

```
PS> 1 -eq 1 
True
```
Some of the most command comparison operators:

```
-eq Compares two values and returns "True" if they are equal

-ne Compares two values and returns "True" if they are not equal

-gt Coompares two values and returns "True" if the first is greater than the second

-ge Compares two values and returns "True" if the first is greater than or equal to the second

-lt  Compares two values and returns "True" if the first is less than the second

-le  Compares two values and returns "True" if the first is less than or equal to the second

-contains  Returns "True" if the first value is contained within the second. This can be used to determined if a value is inside of an array.
```

Test if a computer is online and return "True" if the computer is online:
```
PS> Test-Connection -ComputerName 192.168.0.1 -Quiet -Count 1
True
```

Test if a computer is offline and return "True" if the computer is offline:
```
-not(Test-Connection -ComputerName 192.168.0.1 -Quiet -Count 1)
```
# 'if' Statement

Basic syntax of an "if" statement:
```
if (condition){
	# code to run if the condition evaluates to be true
}
```
 
 - Read the AppConfig.txt file on single server from an array if the server is online:
 ```
$servers = @('serv1', 'serv2', 'serv3', 'serv4')
if (Test-Connection -ComputerName $servers[0] -Quiet -Count 1) {
	Get-Content -Path "\\$($servers[0])\c$\AppConfig.txt"
}
```

```
$servers = @('serv1', 'serv2', 'serv3', 'serv4')
if (Test-Connection -ComputerName $servers[0] -Quiet -Count 1) {
	Get-Content -Path "\\$($servers[0])\c$\AppConfig.txt"
} else { 
	Write-Error -Message "The server $servers[0] is not responding!"
}
```


# "switch" Statement

- a switch statement allows you to execute various pieces of code based on a value

```
switch (expression) {
	expressionvalue {
		# Do something with code here
	}
	expressionvalue {
	}
	default {
		# Stuff to do if no matches were found
	}
}
```

- Use "break" to force Powershell to stop evaluating conditions
- The "break" keyword can be used to make your switch conditions mutually exclusive

```
$currentServer = $servers[0]
switch ($currentServer) {
	$servers[0] {
		# Check if the server is online and get the content at 'serv1' path
		break
	}
	$servers[1]{
		# Check if the server is online and get the context at 'serv2' path
		break
	}
}
``` 

# Using Loops

- A foreach loop can be used in three different ways:
	- as a foreach statement
	- as a ForEach-Object cmdlet
	- as a foreach() method

- Foreach statement:

```
$servers = @('serv1', 'serv2', 'serv3', 'serv4')
foreach ($server in $servers) {
	Get-Content -Path "\\$server\c$\configuration.txt"
}
```
- ForEach-Object cmdlet:
	- Because ForEach-Object is a cmdlet you have to pass a set of objects and an action to complete as parameters

```
$servers = @('serv1', 'serv2', 'serv3', 'serv4')
ForEach-Object -InputObject $servers -Process {
	Get-Content -Path "\\$_\c$\configuration.txt"
}
```
 - You have to use $_ instead of $server to represent the current object in the pipeline

 ```
$servers = @('serv1', 'serv2', 'serv3', 'serv4')
$servers | ForEach-Object -Process {
	Get-Content -Path "\\$_\c$\configuration.txt"
}
```

- The foreach() method:

```
$servers = @('serv1', 'serv2', 'serv3', 'serv4')
$servers.foreach({Get-Content -Path "\\$_\c$\configuration.txt"})
```

- The for loop:
	- Used to execute code a predetermined number of a times

Basic syntax:
```
for ($i = 0; $i -lt 10; $i++) {
	$i
}
```
- A for loop consists of four pieces:
	- The "iteration variable" decleration
	- The condition to continue running the loop
	- The action to perform on the iteration variable after each successful loop
	- The code you want to execute

- For the example above:
	- Start the loop by initializing the variable $i to 0
	- Check to see if $i is less than 10
		- If it is, you execute the code in the curly brackets
	- After the code is executed you increment $i by 1 and check to see if $i is still less than 10
	- Repeat the process until $i is no longer less than 10

```
$servers = @('serv1', 'serv2', 'serv3', 'serv4')
for ($i = 0; $i -lt $servers.Length; $i++) {
	$servers[$i] = "new $server"
}
$servers
```	  

```
$servers = @('serv1', 'serv2', 'serv3', 'serv4')
for ($i = 1; $i -lt $servers.Length; $i++) {
	Write-Host $servers[$i] "comes after" $servers[$i-1]
}
```

- The while loop:
	- The while loop is the simplest loop
	- While a condition is true, do something

```
$counter = 0
while ($counter -lt 10) {
	$counter
	$counter++
}
```
Read a file on a server if it is online:

```
while (Test-Connection -ComputerName $server -Quiet -Count 1) {
	Get-Content -Path "\\$server\c$\config.txt"
	break
}
```

- The do/while and do/until loops:

```
do {

	} while ($true)
```

```
do {

	} until ($true)
```

```
do {
	$choice = Read-Host -Prompt 'What is the best programming language?'
	} until ($choice -eq 'Powershell')
Write-Host -Object 'Correct!'
```

