## Parsing Structured Data

# CSV Files

 - The "Import-Csv" command reads data and transforms it into a Powershell object
```
Import-Csv -Path <path to csv> 
```
Example Employees.csv:

| First Name | Last Name | Department |  Manager   |
|------------|-----------|------------|------------|
| Adam       | Bertram   | IT         | Lisa Smith |
| John       | Anderson  | Admins     | Jeff Baker |
| Jane       | Anderson  | Marketing  | Colin Lions|


```
$firstCsvRow = Import-Csv -Path ./Employees.csv | Select-Object -First

$firstCsvRow | Select-Object -ExpandProperty 'First Name'

Adam
```
# Turning raw data into objects

- 'Import-Csv' puts the content of the file into an array of PSCustomObjects.

Finding users with the last name 'Anderson':
```
Import-Csv -Path ./Employees.csv | Where-Object { $_.'Last Name' -eq 'Anderson'}

| First Name | Last Name | Department |  Manager   |
|------------|-----------|------------|------------|
| John       | Anderson  | Admins     | Jeff Baker |
|------------|-----------|------------|------------|
| Jane       | Anderson  | Marketing  | Colin Lions|
|------------|-----------|------------|------------|
```

Specifying different delimeters (tab):
```
Import-Csv -Path ./Employees.csv -Delimeter "`t"
```

# Building a computer inventory report

Sending a ping and resolving the name of a list of ip addresses within a csv:
```
$rows = Import-Csv -Path C:\IPAddresses.csv

Test-Connection -ComputerName $row[0].IPAddress -Count 1 -Quiet
(Resolve-DnsName -Name $row[0].IPAddress -ErrorAction Stop).Name
``` 
 1. Test the connection. 
 	- "Quiet" will only return true/false if the host is up
 	- "Count" sends one ICMP packet to the host
 2. Resolve the DnsName and only return the "Name" property

Looping:
```
foreach ($row in $rows) {
	Test-Connection -ComputerName $row.IPAddress -Count 1 -Quiet
	(Resolve-DnsName -Name $row.IPAddress -ErrorAction Stop).Name
}
```
 - This will ping and resolve all names, but does not show which IP address is associated with each output
 - To see the association, you will need a hashtable for each row and assign your own elements

Example with hashtable:
```
$rows = Import-Csv -Path C:\IPAddresses.csv

foreach ($row in $rows) {
	try{
		$output = @{
			IPAddress = $row.IPAddress
			Department = $row.Department
			IsOnline = $false
			HostName = $null
			Error = $null
		}
		if (Test-Connection -ComputerName $row.IPAddress -Count 1 -Quiet) {
			$output.IsOnline = $true
		}
		if ($hostname = (Resolve-DnsName -Name $row.IPAddress -ErrorAction Stop).Name) {
			$output.Hostname = $hostname
		}
	} catch {
		$output.Error = $_.Exception.Message
	} finally {
		[pscustomobject]$output
	}
}
```
 
 1. Create a hashtable with values corresponding to the row's columns and the extra information we are looking for

 2. Test whether a computer is connected by pinging the ip address

 3. If the computer is online, set 'IsOnline' to 'True'

 4. Then do the same with the hostname, testing whether it's found
 	- Update the hashtable if the hostname is found
 5. If any errors are found, record them in the hashtable's 'Error' value

 6. Turn the hashtable into a 'PSCustomObject' and return it (regardless of whether an error is thrown)

 7. The whole function is wrapped in a try/catch block


Record the output to a csv:
```
[PSCustomObject]$output | Export-Csv -Path C:\DeviceDiscovery.csv -Append -NoTypeInformation
```
 - By default, "Export-Csv" overwrites the csv, so the 'Append' parameter is specified

# Excel Spreadsheets

 - You can create an excel spreadsheet with the "Export-Excel" command

```
Get-Process | Export-Excel .\Processes.xlsx
```
 - You can use the 'WorkSheetName' parameter to create additional worksheets

```
Get-Process | Export-Excel .\Processes.xlsx -WorkSheetName 'Worksheet2'
```
Reading Excel:
```
Import-Excel -Path .\Processes.xlsx
```
Find all of the worksheets in a workbook:
```
Get-ExcelSheetInfo -Path .\Processes.xlsx
```
Output data from all worksheets:
```
$excelSheets = Get-ExcelSheetInfo -Path .\Processes.xlsx
Foreach ($sheet in $excelSheets) {
	$workSheetName = $sheet.Name
	$sheetRows = Import-Excel -Path .\Processes.xlsx -WorkSheetName
	$workSheetName
	$sheetRows | Select-Object -Propertyv *,@{'Name'='Worksheet';'Expression'={ $workSheetName}}
}
```
 - A calculated proprty is used with "Select-Object"
 	- When using a calculated property, you provide "Select-Object" with a hashtable containing the name of the property to return and an expression that runs when "Select-Object" receives the input

 - By default, "Import-Excel" does not add the worksheet name as a property to each object

# Adding to Excel spreadsheets

An example on how to export all running processes over a period of time:
```
Get-Process | Select-Object -Property *, @{Name = 'Timestamp';Expression = {Get-Date -Format 'MM-dd-yy hh:mm:ss' }} | Export-Excel .\Processes.xlsx -WorkSheetName 'ProcessesOverTime'
```
 - This command can be run multiple times with the 'Append' parameter 

# Creating a Windows service monitoring tool

 - The goal is to build a process to track Windows service stated over time and record the output to an excel sheet

 Pulling all windows service and returning their name and state:
 ```
Get-Service | Select-Object -Proprty Name,Status
 ```

 Timestamp each row using a calculated property:
 ```
Get-Service | Select-Object -Proprty Name,Status,@{Name = 'Timestamp';Expression = { Get-Date -Format 'MM-dd-yy hh:mm:ss' }} | Export-Excel .\ServicesStates.xlsx -WorksheetName 'Services'
 ```

 You can spot the differences with a pivot table in excel:
 ```
Import-Excel .\ServicesStates.xlsx -WorksheetName 'Services' | Export-Excel -Path .\ServicesStates.xlsx -Show -IncludePivotTable -PivotRows Name,Timestamp -PivotData @{Timestamp = 'count'} -PivotColumns Status 
 ```
 - See more at the "Import-Excel" repo - https://github.com/dfinke/ImportExcel

# JSON Data

...