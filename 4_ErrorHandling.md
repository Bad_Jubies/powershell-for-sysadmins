## Error Handling 

 - An "exception" is when your code encounters a problem that disrupts the normal flow 
 	- once an exception is thrown, Powershell will display an error to the user if nothing is done 

 - Terminating Error = any error that stops the execution of code 
 - Nonterminating Error = code execution continues

 - To turn a nonterminating error into a terminating error, you can use the 'ErrorAction' common parameter
 	- a common parameter means that it is found in every cmdlet
 
 - The 'ErrorAction' parameter has five main options:
 	- Continue = Outputs the error message and continues to execute cmdlet. This is the default value.
 	- Ignore = Continues to execute the cmdlet without outputting an error or recording it to the $Error variable.
 	- Inquire = Outputs the error message and prompts the user for input before continuing
 	- SilentlyContinue = Continues to execute the cmdlet without outputting an error, but records to the $Error variable.
 	- Stop = Outputs the error message and stops the cmdlet from executing.

 - To prevent terminating errors from stopping a program, you need to "catch" them. 
 	- You can do so with the Try/Catch/Finally construct

Syntax:
```
try {
	# initial code
} catch {
	# code that runs if terminating error is found
} finally {
	# code that runs at the end
}
```

Example:
```
$folderPath = '.\FakeFolder'
try{
	$files = Get-ChildItem -Path $folderPath -ErrorAction Stop
	$files.foreach({
		$fileText = Get-Content $files
		$fileText[0]
		})
} catch {
	$_.Exception.Message
}
```
 - When a terminating error is caught in the "catch" block, the error object is stored in the $_ variable
 	- Try/Catch can find only terminating errors

 - The $Error variable stores errors in an array
 	- you can user $Error[0], $Error[1], ... to access each individual error
 - 