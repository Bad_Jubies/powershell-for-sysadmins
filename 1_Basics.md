## Getting Started 

PS> Get-Command 
	- get a list of commands that powershell is aware of by default

- Most commands follow the same schema "Verb-Noun"
 	- Most  of the built-in commands from Microsoft are called "cmdlets" => typically written in other languages, like C#

- "Functions" are commands written in Powershell



```
PS C:\Windows\system32> Get-Command -Verb Get -Noun Content                                                              Cmdlet         Get-Content  3.1.0.0    Microsoft.PowerShell.Management
```

## Basic Powershell Concepts

Defining Variables

```
PS> Set-Variable -Name Color -Value Blue

PS> Get-Variable -Name Color

Name        Value
-----       ------
Color       Blue
```
OR

```
PS> $Color = 'Blue'

PS> echo $Color
Blue
```

- In Powershell, everything is an object. An object is an individual instance of a specific template, called a class. A class specifies the kind of things an object will contain. 

- An object's class defines its methods, or actions that can be taken on that object

```
PS> $color = 'red'

PS> Select-Object -InputObject $color -Property *

Length 
-------
     3

```
 - The variable $color only has a single property
 	- you can use "dot notation" to select different properties

```
PS> $color.length
3
```
 - Get-Member can be used to look at all of the properties and methods of an object 

 - Methods can be referenced with dot notation. 
 	- Methods will always end with a set of parentheses and can take one or more parameters
 ```
PS> Get-Member -InputObject $color -Name Remove

Name     Membertype    Definition
-----    ----------    ----------
Remove   Method         string Remove (int startIndex, int count), string Remove(int startIndex)
 ```
 - There are two definitions, so the method can be used in two ways: with "startIndex" and the "count parameter", or with just "startIndex"

```
PS> echo $color
Red

PS> $color.Remove(1)
R

PS> $color.Remove(1,1)
Rd
``` 
$color.Remove(int startIndex, int count)


# Data Structures 
 - A "data structure" is a way to organize multiple pieces of data.

 DEFINING ARRAYS
 	- define a variable called $colorPicker and assign it to an array that holds four color strings

 ```
PS> $colorPicker = @('blue', 'white' 'yellow', 'black')
PS> $colorPicker
blue
white
yellow
black

PS> $colorPicker[0]
blue

PS> $colorPicker[0,1]
blue
white
 ``` 
 - When working with large data sets, it is better to use an ArrayList. Array lists allow you to add and subtract from the array without a large hit to performance. 

 ```
PS> $colorPicker = [System.Collections.ArrayList]@('blue','white','yellow','black')

PS> $colorPicker
blue
white
yellow
black

 ```
Adding to the array: 
```
PS> $colorPicker.Add('gray')
4
```
- 4 is the index of the newly added element

```
PS> $null = $colorPicker.Add('gray')
```
 - This will send the output to $null and no output will be displayed 

```
PS> $null = $colorPicker.Remove('gray')
```

# Hashtables

 - A hashtable is a data structure that contains a list of "key-value pairs"
 	- Instead of using a numeric index, you give Powershell an input, called a key, and it returns a value associated with the key. 

```
PS> $users = @{
	bjohnson = 'Ben Johnson'
	csmith = 'Colin Smith'
	jdoe = 'John Doe'
}

PS> $users

Name           Value
-------        -------
bjohnson       Ben Johnson
csmith         Colin Smith
jdoe           John Doe

```

- Powershell does not allow you to define a hashtable with duplicate values
	- You can define a key within a hashtable to point to an array or another hashtable 

There are two ways to access a value within a hashtable: 

```
PS> $users['jdoe']
John Doe

PS> $users.jdoe
John Doe

``` 

Accessing the "Keys" and "Values" properties:

```
PS> $users.Keys
bjohnson       
csmith         
jdoe           

PS> $users.Values
Ben Johnson
Colin Smith
John Doe

```
Add to a hashtable:
```
$users.Add('ghume', 'Grant Hume')
```
OR 

```
$users['ghume'] = 'Grant Hume'
```
Remove fro ma hashtable:
```
$users.Remove('ghume')
```

# Creating Custom Objects

```
PS> $CutomObject = New-Object -TypeName PSCustomObject
```
```
PS> $CutomObject = PSCustomObject]@{OSBuild = 'x'; OSVersion = 'y'}

PS> $CutomObject.OSBuild
x
PS> $CustomObject.OSVersion
y
```



