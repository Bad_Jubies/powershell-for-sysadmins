$comp = Read-Host -Prompt "Computer Name"

function Get-TSSessions {
    param(
        $ComputerName = $comp
    )

    psexec \\$ComputerName query user |
    ForEach-Object {
        
        $_ = $_.trim() 
        $_ = $_.insert(22,",").insert(42,",").insert(47,",").insert(56,",").insert(68,",")
        $_ = $_ -replace "\s\s+",""
        $_

    } |
    #Convert to objects
    ConvertFrom-Csv
}

Get-TSSessions 

$user = Get-TSSessions
net user $user.username /domain | Write-Host -ForegroundColor Green

wmic.exe process call create "C:\Program Files\UltraVNC\vncviewer.exe $comp"  

