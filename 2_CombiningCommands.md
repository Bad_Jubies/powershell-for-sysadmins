## Combining Commands

# Using the pipeline

```
PS> Get-Service -Name Spool*

Status       Name     DisplayName
-------      ------   ------------
Running      Spooler  Printer Spooler

PS> Get-Service -Name Spool* | Stop-Service 

```
OR

```
'Spool*' | Get-Service | Stop-Service
```

From a list:
```
PS> Get-Content -Path C:\Services.txt
Spooler

PS> Get-Content -Path C:\Services.txt | Get-Service | Stop-Service

``` 

# Writing Scripts 

 - Scripts are external files that store a sequence of commands

 - By default, Powershell does not allow you to run any scripts. This restriction is called "execution policy"
 	
 	- Rescricted: this is the default configuration and does not allow for scripts to be run
 	
 	- AllSigned: This configuration allows you to run only scripts that have been cryptographically signed by a trusted party

 	- RemoteSigned: This configuration allows you to run any script that you write or download as long as it has been signed cryptogrpahically by a third party

 	- Unrestriced: This configuration allows you to run all scripts

 ```
PS> Get-ExecutionPolicy

PS> Set-ExecutionPolicy -ExecutionPolicy Unrestricted

 ```