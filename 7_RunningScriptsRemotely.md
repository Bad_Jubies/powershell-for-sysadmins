## Running Scripts Remotely 

- You can run scripts remotely with "PowerShell Remoting" 
	- similar to sysinternal's psexec

# Working with scriptblocks

- Scriptblocks are code packaged into a single executable unit

- Scriptblocks differ from functions in two main ways:
	- They are anonymous-or unnamed
	- They can be assigned to variables 

Function:
```
function New-Thing {
	param()
	Write-Host "I am in New-Thing!"
}
```
```
PS> New-Thing

I am in New-Thing!
```


Scriptblock:
```
$newThing = {Write-Host "I am in Scriptblock!"}
```
```
PS> $newThing

Write-Host "I am in Scriptblock!"
```
 - You need to add an ampersand to execute the scriptblock:
 ```
PS> & $newThing

I am in Scriptblock!
 ```

# Using Invoke-Command

- You can execute "ad hoc commands" to execute simple expressions or you can create an interactive session

- In order for "Invoke-Command" to work, both computers must be part of the same domain and the computer issuing the commands must have admin rights to the computer receiving the commands

Example:
```
PS> Invoke-Command -ScriptBlock { hostname } -ComputerName WEBSRV1

WEBSRV1
```

Running local scripts on remote servers"
```
PS> cat C:\script.ps1
hostname

PS> Invoke-Command -ComputerName WEBSRV1 -FilePath C:\script.ps1

WEBSRV1
```

# Using local variables remotely

- You cannot use local variables on remote servers by default

```
PS> $ServerFilePath = 'C:\servers.txt'
```
```
PS> Invoke-Command -ComputerName WEBSRV1 -Scriptblock { Write-Host "The servers are in $ServerFilePath" }

The servers are in
```

- You can pass local variables with the "ArgumentList" parameter:
```
PS> Invoke-Command -ComputerName WEBSRV1 -Scriptblock { Write-Host "The servers are in $($args[0])" } -ArgumentList $ServerFilePath

The servers are in C:\servers.txt
```

- Using the $Using statement to pass values:
```
PS> Invoke-Command -ComputerName WEBSRV1 -Scriptblock { Write-Host "The servers are in $using:ServerFilePath"} 

The servers are in C:\servers.txt
```

# Working with sessions

 - Create a remote session with "New-PSSesssion"
 - To enter an interactive session: 
```
Enter-PSSession -ComputerName <computer>
```

Disconnecting:
```
Get-PSSession | Disconnect-PSSession
```
Reconnecting:
```
Connect-PSSession -ComputerName <computer>
```
To clean up old sessions:
```
Get-PSSession | Remove-PSSession
```
- You will need to authenticate if the two computers are on different domains
- You will need to authenticate if the computer does not have powershell remoting enabled

- You can authenticate with CredSSP and Kerberos 
	- CredSSP does not require Active Directory for authentication

# The Double Hop Problem

- The "Double Hop Problem" occurs when you try to access remote resources from within a powershell remote session

 



