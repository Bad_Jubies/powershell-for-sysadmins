## Writing Functions

- A function is a labeled piece of code that performs a single task 
	- functions solve a single problem 
	- anytime you run into a recurring problem, you call the function that you have already called to address it 

# Functions vs cmdlets
 
 - A cmdlet isn't written in powershell - it is typically written in C or C# and then compiled into a binary
 - Functions are written entirely in Powershell

You can see which cmdlets and functions are available:
```
Get-Command 

Get-Command -CommandType function

Get-Command -CommandType cmdlet
```
# Defining a Function

- A function has to be defined before you can use it 

```
PS> function Hello-World { Write-Host 'Hello World!'}

PS> Hello-World
Hello World!
```
- Functions can be named anything, but should follow Powershell's verb-noun syntax
	- The command "Get-Verb" will display a list of recommended verbs

# Adding Parameters to Functions

- Creating a paramter on a function required a "param" block, which holds the parameters for the function

```
function Install-Software {
	[CmdletBinding()]
	param()

	Write-Host 'I installed software version 2.0!'
}
```
- You can create a parameter by putting it within the "param" block's parentheses

```
function Install-Software {
	[CmdletBinding()]
	param(
		[Parameter()]
		[string] $Version
		)

		Write-Host "I installed software version $Version !"
}
```
	- The empty parameter block in this example does nothing, but is required to create the parameter

Within the param bock:
	- Defined the parameter block with [Parameter()]
	- Defined The parameter type with [string]
		- This casts the parameter so Powershell will always try to convert any value that's passed into a string
	- Added $Version to the print statemtent 
		- The version will be printed if 'Install-Software' is run with the Version parameter

```
PS C:\Windows\system32> function Install-Software{
>> [CmdletBinding()]
>> param(
>> [Parameter()]
>> [string] $Version
>> )
>>
>> Write-Host "I installed software version $Version !"
>> } 

PS C:\Windows\system32> Install-Software                                                                                    I installed software version  !

PS C:\Windows\system32> Install-Software -Version 2.5                                                                      I installed software version 2.5 !

```	

# The Mandatory Parameter Attribute

- You can use the "Mandatory" attribute to require the passing of a parameter when calling a funcion

```
function Install-Software {
	[CmdletBinding()]
	param(
		[Parameter(Mandatory)]
		[string] $Version
		)

		Write-Host "I installed software version $Version !"
}
```

Calling the function without supplying any parameter:
```
PS C:\Windows\system32> Install-Software

cmdlet Install-Software at command pipeline position 1
Supply values for the following parameters:
Version: 5

I installed software version 5 !
```
# Default Parameter Values

- You can assign a parameter a default value when a parameter is defined:
```
function Install-Software {
	[CmdletBinding()]
	param(
		[Parameter()]
		[string] $Version = 2
		)

		Write-Host "I installed software version $Version !"
}
```
 - This does not prevent you from passing a parameter
 	- you may use this if you expect version 2 of the software to be installed most of the time 

# Adding Parameter Validation Attributes 

- You can use "Parameter validation attributes" to restrict parameters to certain values

```
function Install-Software {
	[CmdletBinding()]
	param(
		[Parameter(Mandatory)]
		[string]$Version
		)

		Get-ChildItem -Path \\UNC\Path\To\SoftwareV$Version
}
```
 - In this example, the user input will have to complete the file path or the code execution will fail

 - The "ValidateSet" attribute allows you to specify a list of values allowed for a parameter

```
function Install-Software {
	[CmdletBinding()]
	param(
		[Parameter(Mandatory)]
		[ValidateSet('1','2')]
		[string]$Version
		)

		Get-ChildItem -Path \\UNC\Path\To\SoftwareV$Version
}
```

# Accepting Pipeline Input

```
function Install-Software {
	[CmdletBinding()]
	param(
		[Parameter(Mandatory)]
		[ValidateSet('1','2')]
		[string] $Version,
		
		[Parameter(Mandatory)]
		[string] $computerName
		)
		Write-Host "I installed software version $Version on $computerName !"
}
```

Using the function with a foreach loop:
```
$computers = @("Server1", "Server2", "Server3")

function Install-Software {
	[CmdletBinding()]
	param(
		[Parameter(Mandatory)]
		[ValidateSet('1','2')]
		[string] $Version,
		
		[Parameter(Mandatory)]
		[string] $computerName
		)
		Write-Host "I installed software version $Version on $computerName !"
}

foreach ($pc in $computers) {
	Install-Software -Version 2 -computerName $pc 
}
```
# Making the function pipeline compatible 

- You need to decide which type of pipeline input you want your function to accept

- To add pipeline support, you add a parameter attribute:
	- "ValueFromPipeline"
	- "ValueFromPipelineByPropertyName"

```
$computer = "Server1"

function Install-Software {
	[CmdletBinding()]
	param(
		[Parameter(Mandatory)]
		[ValidateSet('1','2')]
		[string] $Version,
		
		[Parameter(Mandatory, ValueFromPipeline)]
		[string] $computerName
		)
		Write-Host "I installed software version $Version on $computerName !"
}

$computer | Install-Software -Version 2

```
# Adding a Process Block

- To tell Powershell how to execute this function for every object coming in, you must include a "process block"

```
function Install-Software {
	[CmdletBinding()]
	param(
		[Parameter(Mandatory)]
		[ValidateSet('1','2')]
		[string] $Version,
		
		[Parameter(Mandatory, ValueFromPipeline)]
		[string] $computerName
		)
		process{
			Write-Host "I installed software version $Version on $computerName !"
		}
		
}

$computers = @("Server1", "Server2", "Server3")
$computers | Install-Software -Version 2

``` 